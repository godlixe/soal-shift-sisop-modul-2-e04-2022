#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <wait.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <dirent.h>
#include <syslog.h>
#include <json-c/json.h>

char path[] = "soal-shift-sisop-modul-2-e04-2022";

char *get_random_file(char *path, int n)
{
    DIR *dp;
    struct dirent *ep;
    int count = 0;

    dp = opendir(path);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)) != NULL)
        {
            if (ep->d_type == DT_REG)
            {
                count++;
                if (count == n)
                    return ep->d_name;
            }
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");

    return 0;
}

struct tm *formatTime(struct tm *inputTime, int addition)
{
    if (inputTime->tm_sec + addition >= 60)
    {
        inputTime->tm_min++;
        inputTime->tm_sec = addition;
        if (inputTime->tm_min >= 60)
        {
            inputTime->tm_hour++;
            inputTime->tm_min = 0;
            if (inputTime->tm_hour >= 24)
            {
                inputTime->tm_hour = 0;
            }
        }
    }
    else
    {
        inputTime->tm_sec += addition;
    }
    return inputTime;
}

void downloadExtract()
{
    char *urls[] = {"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
                    "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};

    char *filename[] = {"characters.zip", "weapons.zip"};
    id_t child_id;
    int status;

    if ((child_id = fork()) == 0)
    {
        execlp("mkdir", "mkdir", "-p", "gacha_gacha", NULL);
    }

    for (int i = 0; i < 2; i++)
    {
        if ((child_id = fork()) == 0)
        {
            execlp("wget", "wget", "--no-check-certificate", urls[i], "-O", filename[i], "-q", NULL);
        }

        while ((wait(&status)) > 0)
            ;

        if ((child_id = fork()) == 0)
        {
            execlp("unzip", "unzip", "-qq", filename[i], NULL);
        }

        while ((wait(&status)) > 0)
            ;

        if ((child_id = fork()) == 0)
        {
            execlp("rm", "rm", "-f", filename[i], NULL);
        }
        while ((wait(&status)) > 0)
            ;
    }
}

char *file_to_json(char *path)
{

    id_t child_id;
    FILE *fp;
    char buffer[10000], *ret;
    ret = malloc(sizeof(char) * 1000);
    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;

    fp = fopen(path, "r");
    fread(buffer, 10000, 1, fp);
    fclose(fp);
    parsed_json = json_tokener_parse(buffer);

    json_object_object_get_ex(parsed_json, "rarity", &rarity);
    json_object_object_get_ex(parsed_json, "name", &name);

    sprintf(ret, "%s_%s", json_object_get_string(rarity), json_object_get_string(name));
    return ret;
}

void gacha(int gacha_counter)
{
    id_t child_id;
    int status;
    if (gacha_counter == 0)
    {
        if ((chdir("gacha_gacha")) < 0)
        {
            exit(EXIT_FAILURE);
        }
    }
    else if (gacha_counter != 0)
    {
        if ((chdir("../")) < 0)
        {
            exit(EXIT_FAILURE);
        }
    }

    int primo_avail;

    time_t currentTime;
    time(&currentTime);
    struct tm *myTime = localtime(&currentTime);

    char dir_name[22] = "total_gacha_", buffer[10];
    snprintf(buffer, 10, "%d", gacha_counter + 90);
    strcat(dir_name, buffer);

    if (gacha_counter % 90 == 0)
    {
        if ((child_id = fork()) == 0)
        {
            execlp("mkdir", "mkdir", "-p", dir_name, NULL);
        }

        while ((wait(&status)) > 0)
            ;
    }

    if (gacha_counter % 10 == 0)
    {
        char r_dir_name[30] = "";
        strcat(r_dir_name, dir_name);
        if ((chdir(r_dir_name)) < 0)
        {
            exit(EXIT_FAILURE);
        }
        srand(time(NULL));
        for (int i = 1; i < 10; i++)
        {
            char file_name[25], buffer2[10];
            struct tm *formattedTime = formatTime(myTime, 1);
            strftime(file_name, sizeof(file_name), "%H:%M:%S", formattedTime);
            strcat(file_name, "_gacha_");
            snprintf(buffer2, 10, "%d", (i * 10) + gacha_counter);
            strcat(strcat(file_name, buffer2), ".txt");

            if ((child_id = fork()) == 0)
            {
                execlp("touch", "touch", file_name, NULL);
            }
            while ((wait(&status)) > 0)
                ;

            FILE *fp;
            fp = fopen(file_name, "a");
            for (int j = 1; j <= 10; j++)
            {
                primo_avail = 79000 - 160 * ((j) + (i * 10) + gacha_counter - 10);
                if (primo_avail < 0)
                    break;
                if (j % 2 != 0)
                {
                    char json_path[70] = "../../characters/";
                    strcat(json_path, get_random_file("../../characters", rand() % (48 - 1) + 1));
                    fprintf(fp, "%d_characters_%s_%d\n", (j) + (i * 10) + gacha_counter - 10, file_to_json(json_path), primo_avail);
                }
                else
                {
                    char json_path[70] = "../../weapons/";
                    strcat(json_path, get_random_file("../../weapons", rand() % (130 - 1) + 1));
                    fprintf(fp, "%d_weapons_%s_%d\n", (j) + (i * 10) + gacha_counter - 10, file_to_json(json_path), primo_avail);
                }
            }
            fclose(fp);
            if (primo_avail < 0)
            {
                return;
            }
        }
    }
}

int main()
{
    pid_t pid, sid;
    pid = fork();

    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/alex/soal-shift-sisop-modul-2-e04-2022/soal1")) < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1)
    {
        id_t child_id;
        int status;

        time_t currentTime;
        time(&currentTime);
        struct tm *currTime = localtime(&currentTime);
        char time_var[15];
        strftime(time_var, sizeof(time_var), "%H:%M:%S_%d-%m", currTime);
        if (strcmp(time_var, "04:44:00_30-03") == 0)
        {
            int primo = 79000, i = 0;
            downloadExtract();
            while (primo > 0)
            {
                gacha(i);
                i += 90;
                primo -= 14400;
            }
        }
        else if (strcmp(time_var, "07:44:00_30-03") == 0)
        {
            if ((chdir("../../")) < 0)
            {
                exit(EXIT_FAILURE);
            }
            char cwd[PATH_MAX];
            if (getcwd(cwd, sizeof(cwd)) != NULL)
            {
                printf("Current working dir: %s\n", cwd);
            }
            else
            {
                perror("getcwd() error");
            }
            if ((child_id = fork()) == 0)
            {
                execlp("zip", "zip", "-P", "satuduatiga", "-r", "not_safe_for_wibu", "-q", "gacha_gacha", NULL);
            }

            while ((wait(&status)) > 0)
                ;

            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "gacha_gacha", NULL);
            }

            while ((wait(&status)) > 0)
                ;

            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "characters", NULL);
            }
            while ((wait(&status)) > 0)
                ;

            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "weapons", NULL);
            }

            while ((wait(&status)) > 0)
                ;
        }
        sleep(1);
    }
}