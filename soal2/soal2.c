#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>

char filename[]="drakor.zip";
char path[]="/home/bagus/shift2/drakor";
	
void unzipfile()
{
	
	id_t child_id;
	int status;
	
	if((child_id=fork())==0)
	{
		execlp("mkdir","mkdir","-p",path,NULL);
	}
	
	while ((wait(&status))>0)
	;
	
	if((child_id=fork())==0)
	{
		execlp("unzip","unzip","-qq",filename,"-d",path,NULL);
	}
	while ((wait(&status))>0)
	;
}

void rmfile(){
   struct dirent *dp;
   DIR *folder;
   folder = opendir(path);

   if(folder != NULL){ 
       while((dp = readdir(folder)) != NULL){
            if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
                 if(dp->d_type == DT_DIR){
                 
			char pathrm[400];
			sprintf(pathrm, "%s/%s", path, dp->d_name);

			id_t child_id;
			int status;
			
			if((child_id=fork())==0)
			{
				execlp("rm","rm","-r",pathrm,NULL);
			}
			
			while ((wait(&status))>0)
			;
                 }
            }
       }
   }
}


void kategori(char *arrpath, char *filename)
{
	id_t child_id;
	int status;
	char *judul = strtok(filename, ";");
	char *tahun = strtok(NULL, ";");
	char *genre = strtok(NULL, ";");
	char *genrenopng = strtok(genre, ".");
	
	char pathkategori[250],pathfilename[250],pathtxt[250];
	
	sprintf(pathkategori, "%s/%s", path, genrenopng);
	sprintf(pathfilename, "%s/%s/%s.png", path, genre,judul);
	sprintf(pathtxt, "%s/%s/data.txt", path, genre);
	
	
	if((child_id=fork())==0)
	{
		execlp("mkdir","mkdir","-p",pathkategori,NULL);
	}
	
	while ((wait(&status))>0)
	;
	
	if((child_id=fork())==0)
	{
		execlp("cp","cp",arrpath,pathfilename,NULL);
	}
	
	while ((wait(&status))>0)
	;
	
	char kategoritxt[300];
	char isitxt[300];
	sprintf(isitxt, "kategori : %s \nnama : %s\nrilis : tahun %s\n\n", genre, judul,tahun);
	
	FILE *filetxt;
	filetxt = fopen(pathtxt, "a");
	
	if(filetxt) {
        fprintf(filetxt, "%s", isitxt);
        fclose(filetxt);
    }
}

int main () {
  	
	unzipfile();
	rmfile();
  	
    char temp[400];
    
    strcpy(temp, path);
    strcat(temp, "/");	
    struct dirent *dp;
    DIR *folder;
    folder = opendir(temp);

    if(folder != NULL){ 
       while((dp = readdir(folder)) != NULL){
            if(strcmp(dp->d_name, ".")&& strcmp(dp->d_name, "..") ){
                 if(dp->d_type == DT_REG){
                      char arrpath[400], filename[400];
                      sprintf(arrpath, "%s/%s",path, dp->d_name);
                      sprintf(filename, "%s",  dp->d_name);
                      

                      char *drakor1 = strtok(filename, "_");
                      char *drakor2 = strtok(NULL, "_");
                      if(drakor2 != NULL){                      
                          kategori(arrpath, drakor2);
                      }
                      kategori(arrpath,drakor1);

                      pid_t pid;
                      pid = fork();
                      if(pid == 0){
                         char *argv[] = {"rm", "-rf", arrpath, NULL};
                         execv("/usr/bin/rm", argv);  
                      }
                      while(wait(NULL) != pid);
                 }
            }
            
       }
   }

  }

